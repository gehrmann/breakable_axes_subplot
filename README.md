**Implements breakable Y-axis for matplotlib/pyplot**

---

## Examples

![Example #1](example-1.png)

![Example #2](example-2.png)

## Usage

To insert break, instead of:

	figure = pyplot.figure()
	layout = gridspec.GridSpec(1, 3)
	plot = figure.add_subplot(layout[1])
	plot.plot([1, 2, 3], [1, 4, 9])

use:

	from breakable_axes_subplot import BreakableAxesSubplot

	figure = pyplot.figure()
	layout = gridspec.GridSpec(1, 3)
	plot = BreakableAxesSubplot(
		figure,
		layout[1],
		# y_limits=y_limits,
		y_breaks=(.22, .78),
		y_proportion=(3, 1),
	)
	plot.plot([1, 2, 3], [1, 4, 9])


## FAQ

#### How to force drawing legend on top part of axes?

Instead of

	plot.legend(loc='best')

use

	plot._plots[0].legend(loc='best')

(Change index from 0 to 1 to force drawing on bottom part)