#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python2" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals; del division, unicode_literals
import logging
import os
import signal
import socket
import sys

from matplotlib import (
	gridspec,
	pyplot,
)

if __name__ == '__main__':
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Sets utf-8 (instead of latin1) as default encoding for every IO
	reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/.'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	# Sets default timeout for network requests
	socket.setdefaulttimeout(10)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
# logging.getLogger(__name__).setLevel(logging.DEBUG)


__doc__ = """
"""


class BreakableAxesSubplot(list):
	"""Implements subplot breakable by Y-axis"""

	def __init__(
			self,
			figure, layout,
			y_limits=None,
			y_breaks=None,
			y_proportion=None,
			*args, **kwargs
	):
		self._figure = figure
		self._layout = layout
		self._bottom_y, self._top_y = y_limits or (None, None)
		self._bottom_break, self._top_break = y_breaks or (None, None)
		self._y_proportion = y_proportion = y_proportion or (1, 1)

		# self._sublayout = sublayout = layout.subgridspec(2, 1)
		self._sublayout = sublayout = gridspec.GridSpecFromSubplotSpec(sum(y_proportion), 1, subplot_spec=layout)

		self._plots = plots = []
		self._plots += [figure.add_subplot(sublayout[:y_proportion[1], 0])]
		self._plots += [figure.add_subplot(sublayout[y_proportion[1]:, 0])]

	def __getattr__(self, key):
		def _call(*args, **kwargs):
			for plot in self._plots:
				getattr(plot, key)(*args, **kwargs)
		return _call

	def set_ylabel(self, *args, **kwargs):
		y_proportion = self._y_proportion

		index = 1 if y_proportion[0] > y_proportion[1] else 0

		label_y = .5 * (1 + y_proportion[1] / y_proportion[0])
		self._plots[index].yaxis.label.set_y(label_y)
		return self._plots[index].set_ylabel(*args, **kwargs)

	def legend(self, *args, **kwargs):
		y_proportion = self._y_proportion

		index = 1 if y_proportion[0] > y_proportion[1] else 0

		return self._plots[index].legend(*args, **kwargs)

	def plot(self, *args, **kwargs):
		plots = self._plots
		bottom_break, top_break = self._bottom_break, self._top_break
		bottom_y, top_y = self._bottom_y, self._top_y
		y_proportion = self._y_proportion

		plots[0].set_xlabel('')
		plots[0].spines['bottom'].set_visible(False)
		plots[0].xaxis.tick_top()
		plots[0].tick_params(labeltop=False)  # don't put tick labels at the top

		plots[1].set_title('')
		plots[1].spines['top'].set_visible(False)
		plots[1].xaxis.tick_bottom()

		###
		self.__getattr__('plot')(*args, **kwargs)
		###

		# Gets auto-calculated Y-limits if not set manually
		if bottom_y is None:
			bottom_y = plots[0].get_ylim()[0]
		if top_y is None:
			top_y = plots[0].get_ylim()[1]

		# Configures sub-plots limits for Y-axis
		if bottom_break is None:
			bottom_break = (self._top_y - self._bottom_y) / 2
		plots[1].set_ylim(self._bottom_y, bottom_break)
		if top_break is None:
			top_break = (self._top_y - self._bottom_y) / 2
		plots[0].set_ylim(top_break, self._top_y)

		# Draws diagonal lines
		d = 0.015
		bbox = plots[0].get_window_extent().transformed(self._figure.dpi_scale_trans.inverted())
		scale = bbox.width / bbox.height
		plots[0].plot((-d, +d), (-d * scale, +d * scale), transform=plots[0].transAxes, color='k', clip_on=False)  # top-left diagonal
		plots[0].plot((1 - d, 1 + d), (-d * scale, +d * scale), transform=plots[0].transAxes, color='k', clip_on=False)  # top-right diagonal
		bbox = plots[1].get_window_extent().transformed(self._figure.dpi_scale_trans.inverted())
		scale = bbox.width / bbox.height
		plots[1].plot((-d, +d), (1 - d * scale, 1 + d * scale), transform=plots[1].transAxes, color='k', clip_on=False)  # bottom-left diagonal
		plots[1].plot((1 - d, 1 + d), (1 - d * scale, 1 + d * scale), transform=plots[1].transAxes, color='k', clip_on=False)  # bottom-right diagonal


def run_init():
	"""Default entry point (only for developing purposes)"""
	import argparse
	parser = argparse.ArgumentParser()
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Generates example data
	import numpy as numpy
	data = numpy.array([
		0.015, 0.166, 0.133, 0.159, 0.041, 0.024, 0.195, 0.039, 0.161, 0.018,
		0.143, 0.056, 0.125, 0.096, 0.094, 0.051, 0.043, 0.021, 0.138, 0.075,
		0.109, 0.195, 0.050, 0.074, 0.079, 0.155, 0.020, 0.010, 0.061, 0.008,
	])
	data[[3, 14]] += .8

	figure = pyplot.figure()

	def set_window_geometry(x, y, width, height):
		window = pyplot.get_current_fig_manager().window
		if hasattr(window, 'geometry'):
			window.geometry('{width}x{height}+{x}+{y}'.format(**locals()))
		elif hasattr(window, 'setGeometry'):
			window.setGeometry(x, y, width, height)
	width, height = 1300, 750
	set_window_geometry(64, 20, width, height)

	layout = gridspec.GridSpec(1, 3)  # Rows, cols

	plot = BreakableAxesSubplot(
		figure, layout[0],
		y_breaks=(.22, .78),
		y_proportion=(1, 3),
	)
	plot.plot(data)

	plot = BreakableAxesSubplot(
		figure, layout[1],
		y_breaks=(.22, .78),
		y_proportion=(1, 1),
	)
	plot.plot(data)

	plot = BreakableAxesSubplot(
		figure, layout[2],
		y_breaks=(.22, .78),
		y_proportion=(3, 1),
	)
	plot.plot(data)

	# pyplot.show()
	figure.waitforbuttonpress(0); pyplot.close(figure)  # Shows window with figure till key press


def main():
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Raises verbosity level for script (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs.get('verbose', 0), 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
